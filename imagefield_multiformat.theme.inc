<?php

/**
 * @file
 * Theme implementations for the formatters.
 */

function theme_imagefield_multiformat_formatter_multiformat_image($element) {
  static $static_nid; // At first test this will be unset, so it'll trigger the first pic.

  if ($element['#node']->nid != $static_nid) {
    // Apply the format for the first image.
    $static_nid = $element['#node']->nid;
    $preset = variable_get('multiformat_first', 0);
  }
  else {
    $preset = variable_get('multiformat_others', 0);
  }
  $item = $element['#item'];
  return theme('imagecache', $preset, $item['filepath'], $item['data']['alt'], $item['data']['title']);
}

function theme_imagefield_multiformat_formatter_multiformat_fancybox($element) {
  static $static_nid; // At first test this will be unset, so it'll trigger the first pic.

  if ($element['#node']->nid != $static_nid) {
    // Apply the format for the first image.
    $static_nid = $element['#node']->nid;
    $preset = variable_get('multiformat_first', 0);
  }
  else {
    $preset = variable_get('multiformat_others', 0);
  }
  $item = $element['#item'];
  return theme('imagefield_image_imagecache_fancybox', $preset, $element['#field_name'], $item['filepath'], $item['data']['title']);
}
