<?php

function imagefield_multiformat_settings() {
  $form = array();
  $options = array();
  
  foreach (imagecache_presets() as $preset) {
    $options[$preset['presetname']] = $preset['presetname'];
  }
  
  $form['multiformat_first'] = array(
    '#type' => 'select',
    '#title' => t('Multiformat First'),
    '#multiple' => FALSE,
    '#description' => t('Select the preset for the first image.'),
    '#options' => $options,
    '#default_value' => variable_get('multiformat_first', 0),
  );
  
  $form['multiformat_others'] = array(
    '#type' => 'select',
    '#title' => t('Multiformat Others'),
    '#multiple' => FALSE,
    '#description' => t('Select the preset for the other images.'),
    '#options' => $options,
    '#default_value' => variable_get('multiformat_others', 0),
  );
  

  return system_settings_form($form);
}
